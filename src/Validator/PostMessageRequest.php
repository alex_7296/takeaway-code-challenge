<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 17.04.19
 * Time: 21:53
 */

namespace App\Validator;


use Symfony\Component\Validator\Constraint;

class PostMessageRequest extends Constraint
{
    public $message = '{{ errorText }}';
}