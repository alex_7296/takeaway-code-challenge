<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 17.04.19
 * Time: 22:41
 */

namespace App\Validator;


use Symfony\Component\Validator\Constraint;

class MessageImmutability extends Constraint
{
    public $message = 'This value does not match the saved value.';
}