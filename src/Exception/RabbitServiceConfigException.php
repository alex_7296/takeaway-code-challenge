<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 19.04.19
 * Time: 10:36
 */

namespace App\Exception;


class RabbitServiceConfigException extends RabbitServiceException
{
}