<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;
use JMS\Serializer\Annotation AS JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MessageRepository")
 */
class Message
{
    const STATUS_NEW = 'new';
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    const STATUSES = [
        self::STATUS_NEW,
        self::STATUS_SUCCESS,
        self::STATUS_ERROR,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="uuid", unique=true)
     * @JMS\Type("string")
     */
    private $idempotencyKey;

    /**
     * @ORM\Column(type="datetime")
     * @JMS\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @JMS\Exclude
     */
    private $sent;

    /**
     * @ORM\Column(type="datetime")
     * @JMS\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $deliveryTime;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $restaurantTitle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $error;

    /**
     * Message constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->created = new DateTime();
        $this->status = self::STATUS_NEW;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdempotencyKey(): string
    {
        return $this->idempotencyKey;
    }

    public function setIdempotencyKey(string $idempotencyKey): void
    {
        $this->idempotencyKey = $idempotencyKey;
    }

    public function getCreated(): DateTime
    {
        return $this->created;
    }

    public function setCreated(DateTime $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getSent(): ?DateTime
    {
        return $this->sent;
    }

    public function setSent(DateTime $sent): self
    {
        $this->sent = $sent;

        return $this;
    }

    public function getDeliveryTime(): DateTime
    {
        return $this->deliveryTime;
    }

    public function setDeliveryTime(DateTime $deliveryTime): self
    {
        $this->deliveryTime = $deliveryTime;

        return $this;
    }

    public function getRestaurantTitle(): ?string
    {
        return $this->restaurantTitle;
    }

    public function setRestaurantTitle(string $restaurantTitle): self
    {
        $this->restaurantTitle = $restaurantTitle;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    public function getError()
    {
        return $this->error;
    }

    public function setError($error): void
    {
        $this->error = $error;
    }

}

