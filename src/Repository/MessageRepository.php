<?php

namespace App\Repository;

use App\Entity\Message;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Message|null find($id, $lockMode = null, $lockVersion = null)
 * @method Message|null findOneBy(array $criteria, array $orderBy = null)
 * @method Message[]    findAll()
 * @method Message[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Message::class);
    }

    /**
     * @param string $idempotencyKey
     *
     * @return Message|null
     */
    public function getByIdempotencyKey(string $idempotencyKey): ?Message
    {
        return $this->findOneBy(['idempotencyKey' => $idempotencyKey]);
    }

    public function getSuccessMessagesForMainPage(?\DateTime $from, ?int $limit = 50)
    {
        $qb = $this->getMessagesQueryBuilderForMainPage(Message::STATUS_SUCCESS, $from, $limit);

        return $qb->getQuery()->getResult();
    }

    public function getNotSuccessMessagesForMainPage(?\DateTime $from, ?int $limit = 50)
    {
        $qb = $this->getMessagesQueryBuilderForMainPage(null, $from, $limit);
        $qb->andWhere('m.status != :status');
        $qb->setParameter('status', Message::STATUS_SUCCESS);

        return $qb->getQuery()->getResult();
    }

    public function getMessagesQueryBuilderForMainPage(?string $status, ?\DateTime $from, ?int $limit = 50)
    {
        $qb = $this->createQueryBuilder('m');
        if($status)
        {
            $qb->andWhere('m.status = :status');
            $qb->setParameter('status', $status);
        }
        if($from)
        {
            $qb->andWhere('m.created > :from');
            $qb->setParameter('from', $from);
        }
        $qb->orderBy('m.created', 'DESC');
        $qb->setMaxResults($limit);
        return $qb;
    }
}
