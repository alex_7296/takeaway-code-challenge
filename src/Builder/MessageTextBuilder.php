<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 22.04.19
 * Time: 19:01
 */

namespace App\Builder;


use App\Entity\Message;
use Twig\Environment;

class MessageTextBuilder
{
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function build(Message $message): string
    {
        return $this->twig->render('message.twig', ['message' => $message]);
    }
}
