<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 20.04.19
 * Time: 15:11
 */

namespace App\Service;

interface MessageTransferInterface
{
    public function publish(string $message);

    public function onConsume(callable $callback);
}