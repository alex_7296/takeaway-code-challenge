<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 25.04.19
 * Time: 10:53
 */

namespace App\Service;


use PhpAmqpLib\Connection\AMQPStreamConnection;

class RabbitServiceInitializer
{
    private $host;
    private $user;
    private $password;
    private $port;
    private $queueName;
    private $exchangeName;

    public function __construct(
        string $host,
        string $user,
        string $password,
        int $port = 5672,
        string $exchangeName = 'messages',
        string $queueName = 'messages_to_send'
    ) {
        $this->host = $host;
        $this->user = $user;
        $this->password = $password;
        $this->port = $port;

        $this->exchangeName = $exchangeName;
        $this->queueName = $queueName;
    }

    public function isInitialized(RabbitService $rabbitService): bool
    {
        return $rabbitService->getConnection() && $rabbitService->getChannel();
    }

    public function initialize(RabbitService $rabbitService)
    {
        $connection = new AMQPStreamConnection($this->host, $this->port, $this->user, $this->password);

        $channel = $connection->channel();
        $channel->exchange_declare($this->exchangeName, 'fanout', false, true, false);
        $channel->queue_declare($this->queueName, false, true, false, false);
        $channel->queue_bind($this->queueName, $this->exchangeName);

        $rabbitService->setConnection($connection);
        $rabbitService->setChannel($channel);
        $rabbitService->setExchangeName($this->exchangeName);
        $rabbitService->setQueueName($this->queueName);
    }

}