<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 20.04.19
 * Time: 14:49
 */

namespace App\Service;


use App\Entity\Message;
use App\Exception\MessageSendException;

interface MessageSenderInterface
{
    /**
     * @param Message $message
     *
     * @return mixed
     * @throws MessageSendException
     */
    public function send(Message $message);
}