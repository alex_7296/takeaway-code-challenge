<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 18.04.19
 * Time: 20:51
 */

namespace App\Service;


use App\Entity\Message;
use App\Exception\MessageSendException;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;

class MessageService
{
    private $entityManager;

    private $messageTransfer;

    private $serializer;

    private $messageSender;

    public function __construct(
        EntityManagerInterface $entityManager,
        MessageTransferInterface $messageTransfer,
        SerializerInterface $serializer,
        MessageSenderInterface $messageSender
    ) {
        $this->entityManager = $entityManager;
        $this->messageTransfer = $messageTransfer;
        $this->serializer = $serializer;
        $this->messageSender = $messageSender;
    }


    public function scheduleMessageForSend(Message $message)
    {
        $this->entityManager->persist($message);
        $this->entityManager->flush();

        $serializedMessage = $this->serializer->serialize($message, 'json');

        $this->messageTransfer->publish($serializedMessage);
    }

    public function sendScheduledMessages()
    {
        $this->messageTransfer->onConsume(function(string $serializedMessage){
            $message = $this->serializer->deserialize($serializedMessage, Message::class, 'json');
            $this->sendMessage($message);
        });
    }

    public function sendMessage(Message $message)
    {
        try
        {
            $message->setSent(new \DateTime());
            $this->messageSender->send($message);
            $message->setStatus(Message::STATUS_SUCCESS);
        } catch(MessageSendException $exception) {
            $message->setError($exception->getMessage());
            $message->setStatus(Message::STATUS_ERROR);
        }

        $this->entityManager->persist($message);
        $this->entityManager->flush();
    }
}