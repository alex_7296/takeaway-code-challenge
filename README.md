# Code challenge

The main purpose of this microservice is to send sms messages to customers. It uses [MessageBird](https://www.messagebird.com/en/) 
as message delivering provider. The microservice handles requests through idempotency REST API.

## API endpoint

To send message send request to `/api/v1/message` in JSON format. The JSON should has following fields:

| Field | Format | Description |
| :--- | :--- | :--- |
| `phone` | 11 digits | Customer phone number that will get a sms message |
| `restaurant_title` |text, max length - 255 symbols | The restaurant title. This field will displayed as sms originator |
| `delivery_time` | text, yyyy-MM-dd HH:mm:ss | Delivery time. This date will present in sms text message |
| `idempotency_key` | UUID | This identifier is used to implement idempotency checks. |

##### Request example
```json
{
    "phone": "31755173843",
    "restaurant_title": "Tasty pizza", 
    "delivery_time": "2019-06-04 12:10:00",
    "idempotency_key": "196fef60-98d9-11e3-a5e2-080020219aa5"
}
```
## How it works
![Image](docs/Service_schema.png)

The API client sends request to **nginx** (1). Nginx passes the request to **API backend container**(2) 
that validates request and does idempotecy checks. If request is correct the new message stored to **database**(3) with status _new_.
Also message scheduled for send to the **message queue**(4). There is **sender**(5) service that runs by _supervisor_. The **sender**(5)
consumes messages from the **message queue**(4). Every consumed message is tried to send to customer with _MessageBird_ service.
The sending result is stored to **database**(3).

## Dependencies
You should install only [Docker](https://docs.docker.com/install/linux/docker-ee/ubuntu/) and [Docker-compose](https://docs.docker.com/compose/install/).

## Installation
Installation is very simple. Clone this repository, than make sure that ports available: 
+ **3306** (MySQL)
+ **80** (nginx)
+ **15672** (rabbitmq-management)

After that run (it is advised to not specify **-d** flag at first run)
```bash
docker-compose up
```
The installation process begins. Required docker images will be downloaded and builded. 

The first build can take 
15-25 minutes depends on your network connection and system characteristics. When all images are downloaded and built
the database initialization process starts. It includes database creation and dump import. 

> **_NOTE:_**  Do not stop the *db_master* service until the initialization finished because it can lead the database files to inconsistent state.

This service has two persistent storages: for MySQL and for RabbitMQ. The files stored in `docker/data/mysql` and `docker/data/rabbitmq`

Specify `MESSAGEBIRD_API_TOKEN` in `docker-compose.yml` for message sending.  Remember that using test API TOKENs it 
is possible to send messages only to your own number in [MessageBird](https://support.messagebird.com/hc/en-us/articles/115002601149-Why-can-I-only-send-messages-to-myself-)

## Tests
Run `php bin/phpinut` inside _message_sender_ docker-container for tests.

## Web interface
There is web interface on _http://localhost:80_ where you can see a log of the last 50 sent messages and at the messages 
that returned any other status then delivered in the last 24 hours (`new` and `error`).

Also, there is RabbitMQ management interface on _http://localhost:15672/_. Login: `rabbitmq`, password: `rabbitmq`.