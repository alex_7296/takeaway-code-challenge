<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 25.04.19
 * Time: 22:04
 */

namespace App\Tests\Builder;

use App\Builder\MessageTextBuilder;
use App\Entity\Message;
use PHPUnit\Framework\TestCase;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class MessageTextBuilderTest extends TestCase
{

    public function testBuild()
    {
        $message = new Message();
        $message->setDeliveryTime(new \DateTime('2019-12-14 12:00:00'));
        $messageTextBuilder = new MessageTextBuilder(new Environment(new FilesystemLoader('templates')));
        $this->assertEquals('Your meal will be delivered at 14.12.2019 12:00:00.', $messageTextBuilder->build($message));
    }
}
