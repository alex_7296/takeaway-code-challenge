<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 25.04.19
 * Time: 21:59
 */

namespace App\Tests\Command;

use App\Command\MessageSendCommand;
use App\Service\MessageService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MessageSendCommandTest extends TestCase
{
    public function testExecute()
    {
        $messageService = $this->createMock(MessageService::class);
        $messageService->expects($this->once())->method('sendScheduledMessages');
        $command = new MessageSendCommand($messageService);
        $command->run($this->createMock(InputInterface::class), $this->createMock(OutputInterface::class));
    }
}
